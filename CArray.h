#pragma once

#include <iostream>
#include <exception>

template<class TData> 
class CArray
{
public: 

  using iterator = TData *;
  using const_iterator = const TData *;

  CArray();

  explicit CArray(
      unsigned int _size
    );

  explicit CArray(
      const CArray<TData> & _array
    );

  CArray(
	  const std::initializer_list<TData> & _data
    );
	
  CArray(
      CArray<TData> && _array
    ) noexcept;

  CArray & operator=(
      const CArray<TData> & _array
	);

  CArray & operator=(
     CArray<TData> && _array
    ) noexcept;
	
  ~CArray();

  bool operator==(
      const CArray<TData> & _array
    );

  bool operator!=(
      const CArray<TData> & _array
    );
  // Tests whether container is empty
  bool empty() const noexcept;

  // Returns size of allocated storage capacity
  unsigned int capacity() const noexcept;

  // Returns the number of elements
  unsigned int size() const noexcept;

  /* Requests a change in capacity
  reserve() will never decrase the capacity */
  void reserve(
      unsigned int _newAlloc
    );
	
  /* Changes the containers size.
  If the newsize is smaller, the last elements will be lost.
  Has a default value param for custom values when resizing.*/
  void resize(
      unsigned int _newSize,
      TData _value = TData()
    );

  // Inserts element at the back
  void push_back(
      const TData & _value
    );

  // Removes the last element from the container
  void pop_back();

  // Exchange values this and _otherArray
  void swap(
      CArray & _otherArray
    ) noexcept;

  // Insert _value to _index
  void insert(
      unsigned int _index,
      const TData & _value
    );

  // Removes from the container a single element at a position _index
  void erase(
	  unsigned int _index
    );

  // Removes from the container a single element at a position _pos
  iterator erase(
	  const_iterator _pos
    );

  // Removes from the container a range of elements ([_from, _to))
  iterator erase(
	  const_iterator _from,
	  const_iterator _to
    );

  /* Removes all elements from the container
  Capacity is not changed */
  void clear() noexcept;

  // Access elements with bounds checking
  TData & at(
      unsigned int _index
    );

  // Access elements with bounds checking for constant container.
  const TData & at(
      unsigned int _index
    ) const;

  // Access elements, no bounds checking
  TData & operator[](
      unsigned int _index
    );

  // Access elements, no bounds checking for constant container
  const TData & operator[](
      unsigned int _index
    ) const;

  // Returns a reference to the first element
  TData & front();

  // Returns a reference to the first element
  const TData & front() const;

  // Returns a reference to the last element
  TData & back();

  // Returns a reference to the last element
  const TData & back() const;

  //Iterators
  iterator begin() noexcept;
  const_iterator begin() const noexcept;
  const_iterator cbegin() const noexcept;
  iterator end() noexcept;
  const_iterator end() const noexcept;
  const_iterator cend() const noexcept;

protected: // Attributes
  unsigned int m_capacity;
  unsigned int m_size;
  TData * m_data;

private:
  inline void moveArray(TData * dest, TData * from, unsigned int n);
};

template<typename TData>
CArray<TData>::CArray() 
  : m_capacity()
  , m_size()
  , m_data()
{
  ///
}

template<typename TData>
CArray<TData>::CArray(
    unsigned int _size
  ) : m_capacity(_size + 5)
	, m_size(_size)
	, m_data(new TData[m_capacity])
{
  ///
}

template<typename TData>
CArray<TData>::CArray(
    const CArray<TData> & _array
  ) : m_capacity(_array.m_capacity)
	, m_size(0)
	, m_data(new TData[_array.m_size])
{
  for (const TData & elem : _array)
    {
	  this->push_back(elem);
    }
}

template<typename TData>
CArray<TData>::CArray(
    const std::initializer_list<TData> & _data
  ) : m_capacity(0)
    , m_size(0)
{
  unsigned int count = _data.size();
  reserve(count);
  for ( const TData & elem : _data)
    this->push_back(elem);
}

template<typename TData>
CArray<TData>::CArray(
    CArray<TData> && _array
  ) noexcept 
	: m_capacity(0)
	, m_size(0)
	, m_data(nullptr)
{
  _array.swap( * this);
}

template<typename TData>
CArray<TData>& CArray<TData>::operator=(
    const CArray<TData> & _array
  )
{
  if (this == & _array)
    return * this;

  unsigned int newSize = _array.m_size;
  unsigned int newCapacity = _array.m_capacity;
  TData * newData = new TData[newCapacity];

  for ( auto i = 0; i < newCapacity; ++i)
    newData[i] = _array.m_data[i];

  std::swap(newSize, m_size);
  std::swap(newCapacity, m_capacity);
  std::swap(newData, m_data);

  delete[] newData;

  return *this;
}

template<typename TData>
CArray<TData>& CArray<TData>::operator=(
    CArray<TData> && _array
  ) noexcept
{
  _array.swap(*this);
  return *this;
}

template<typename TData>
CArray<TData>::~CArray()
{
  clear();
}

template<typename TData>
bool CArray<TData>::operator==(
    const CArray<TData> & _array
  )
{
  if (m_size == _array.m_size && m_capacity == _array.m_capacity)
  {
	for ( auto i = 0; i < m_size; ++i)
	{
	  if (m_data[i] != _array.m_data[i])
		return false;
	} 
	return true;
  }
  return false;
}

template<typename TData>
bool CArray<TData>::operator!=(
    const CArray<TData> & _array
  )
{
  return !operator==(_array);
}

template<typename TData>
bool CArray<TData>::empty() const noexcept
{
  return (m_size == NULL);
}

template<typename TData>
unsigned int CArray<TData>::capacity() const noexcept
{
  return m_capacity;
}

template<typename TData>
unsigned int CArray<TData>::size() const noexcept
{
  return m_size;
}

template<typename TData>
void CArray<TData>::reserve(
    unsigned int _newAlloc
  )
{
  if(_newAlloc <= m_capacity) return;

  TData * newData = new TData[_newAlloc];

  for ( auto i = 0; i < m_size; ++i)
    newData[i] = m_data[i];

  delete[] m_data;

  m_data = newData;
  m_capacity = _newAlloc;
}

template<typename TData>
void CArray<TData>::resize(
    unsigned int _newSize,
    TData _value
  )
{
  reserve(_newSize);
  for ( auto i = m_size; i < _newSize; ++i)
    m_data[i] = _value;

  m_size = _newSize;
}

template<typename TData>
void CArray<TData>::push_back(
    const TData & _value
  )
{
  if (m_size == m_capacity)
  {
    m_capacity += 5;
	TData* tempData = new TData[m_capacity];
	for ( auto i = 0; i < m_size; ++i)
      tempData[i] = m_data[i];

    tempData[m_size] = _value;
	delete[] m_data;
	m_data = tempData;
	m_size++;
  }
  else 
  {
    m_data[m_size] = _value;
    m_size++;
  }
}

template<typename TData>
void CArray<TData>::pop_back()
{
  if (m_size > 0)
    --m_size;
  m_data[m_size].TData::~TData();
}

template<typename TData>
void CArray<TData>::swap(
    CArray & _otherArray
  ) noexcept
{
  std::swap(m_data, _otherArray.m_data);
  std::swap(m_size, _otherArray.m_size);
  std::swap(m_capacity, _otherArray.m_capacity);
}

template<typename TData>
void CArray<TData>::insert(
    unsigned int _index,
    const TData & _value
  )
{
  if ((_index < 0) || (_index > m_size))
    throw std::exception("Index out of range");

  if (m_size == m_capacity)
    resize(m_capacity + m_capacity / 2 + 1);

  iterator iter = &m_data[_index];
  moveArray(iter + 1, iter, m_size - (iter - m_data));

  ++m_size;
  m_data[_index] = _value;
}

template<typename TData>
void CArray<TData>::erase(
    unsigned int _index
  )
{
  erase(begin() + _index);
}

template<typename TData>
inline typename CArray<TData>::iterator CArray<TData>::erase(
    const_iterator _pos
  )
{
  iterator iter = &m_data[_pos - m_data];
  iter->TData::~TData();
  moveArray(iter, iter + 1, m_size - (iter - m_data));

  m_size -= 1;

  return iter;
}

template<typename TData>
inline typename CArray<TData>::iterator CArray<TData>::erase(
    const_iterator _from,
    const_iterator _to
  )
{
  auto length = _to - _from;
  iterator iter = &m_data[_from - m_data];
  iterator last = iter + length;
  if (length == 0)
    return iter;

  for ( auto i = 0; i < length; ++i)
    _from++->TData::~TData();

  moveArray(iter, last, m_size - (last - m_data));

  m_size -= length;

  return iter;
}

template<typename TData>
void CArray<TData>::clear() noexcept
{
  for ( auto i = 0; i < m_size; ++i)
    m_data[i].TData::~TData();

  m_size = NULL;
}

template<typename TData>
TData & CArray<TData>::at(
    unsigned int _index
  )
{
  if ((_index < 0) || (_index > m_size))
    throw std::exception("Index out of range");

  return m_data[_index];
}

template<typename TData>
const TData & CArray<TData>::at(
    unsigned int _index
  ) const
{
  if ((_index < 0) || (_index > m_size))
    throw std::exception("Index out of range");

  return m_data[_index];
}

template<typename TData>
TData & CArray<TData>::operator[](
    unsigned int _index
  )
{
  return m_data[_index];
}

template<typename TData>
const TData & CArray<TData>::operator[](
    unsigned int _index
  ) const
{
  return m_data[_index];
}

template<typename TData>
TData & CArray<TData>::front()
{
  if(m_size > 0)
    return m_data[0];
  else
    throw std::logic_error("Container is empty");
}

template<typename TData>
const TData & CArray<TData>::front() const
{
  if (m_size > 0)
    return m_data[0];
  else
    throw std::logic_error("Container is empty");
}

template<typename TData>
TData & CArray<TData>::back()
{
  if(m_size != NULL)
    return m_data[m_size - 1];
  else
	throw std::logic_error("Container is empty");
}

template<typename TData>
const TData & CArray<TData>::back() const
{
  if(m_size != NULL)
    return m_data[m_size - 1];
  else
    throw std::logic_error("Container is empty");
}

template<typename TData>
inline typename CArray<TData>::iterator CArray<TData>::begin() noexcept
{
  return m_data;
}

template<typename TData>
inline typename CArray<TData>::const_iterator CArray<TData>::begin() const noexcept
{
  return m_data;
}

template<typename TData>
inline typename CArray<TData>::const_iterator CArray<TData>::cbegin() const noexcept
{
  return m_data;
}

template<typename TData>
inline typename CArray<TData>::iterator CArray<TData>::end() noexcept
{
  return m_data + m_size;
}

template<typename TData>
inline typename CArray<TData>::const_iterator CArray<TData>::end() const noexcept
{
  return m_data + m_size;
}

template<typename TData>
inline typename CArray<TData>::const_iterator CArray<TData>::cend() const noexcept
{
  return m_data + m_size;
}

template<typename TData>
inline void CArray<TData>::moveArray(TData * dest, TData * from, unsigned int n)
{
  memmove(static_cast<void *>(dest), static_cast<void *>(from), n * sizeof(TData));
}



