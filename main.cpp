#include <iostream>
#include "CArray.h"

#include <random>
#include <string>

class RemoveCondition
{
  int i;
  unsigned int z;

public:
  explicit RemoveCondition(
      unsigned int _z
    ) : i(0)
      , z(_z)
  {
    ///
  }

  ~RemoveCondition() = default;

  bool operator()(
      int
    )
  {
    return ++i % z == 0;
  }
};

template<typename TData>
void print(
    const CArray<TData> & vec
  )
{
  if(vec.size() == NULL)
  {
    std::cout << "Array is empty";
    std::cout << std::endl;
    return;
  }

    for ( const auto & val : vec)
      std::cout << val << ",";
    std::cout << std::endl;
}

int main()
{
  CArray<int> intArray;
  intArray.resize(20);
  std::random_device rd;
  std::mt19937 mt(rd());
  std::uniform_int_distribution<int> dist(0, 100);
    
  std::for_each(intArray.begin(), intArray.end(), [& dist, & mt](int & val) 
  {
    val = dist(mt);
  });

  print(intArray);
  std::sort(intArray.begin(), intArray.end(), [](const int a, const int b)
  {
    return a < b; 
  });
  print(intArray);

  intArray.erase(std::remove_if(intArray.begin(), intArray.end(), RemoveCondition(2)), intArray.end());
  print(intArray);

  int i = 10;
  do
  {
    intArray.insert(rand() % intArray.size(), rand() % 100);
  } while (--i);
  print(intArray);

  intArray.clear();
  print(intArray);

  CArray<std::string> strArray;
  strArray.resize(15);

  const unsigned short maxLength = 10;
  const unsigned short minLength = 4;
  std::string str("abcdefghijklmnopqrstuvwxyz");
 
  std::for_each(strArray.begin(), strArray.end(), [&](std::string & val)
  {
    std::shuffle(str.begin(), str.end(), mt);
    val = str.substr(0, rand() % maxLength + minLength);
  });
  print(strArray);

  std::sort(strArray.begin(), strArray.end());
  print(strArray);

  strArray.erase(std::remove_if(strArray.begin(), strArray.end(), [](const std::string & s) 
  {
    return std::find_if(s.begin(), s.end(), [](const char c) 
    {
      return c == 'a' || c == 'b' || c == 'c' || c == 'd' || c == 'e';
    }) != s.end();
  }), strArray.end());
  print(strArray);

  i = 3;
  do
  {
    auto insertedStr = [&]()
    {
      std::shuffle(str.begin(), str.end(), mt);
      return str.substr(0, rand() % maxLength + minLength);
    }();

    strArray.insert(rand() % strArray.size(), insertedStr);
  } while (--i);
  print(strArray);

  return 0;
}
